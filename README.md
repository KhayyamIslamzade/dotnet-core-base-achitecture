
#Application 

##Architecture

#### Application
- Services
- Middlewares

#### Core
- BaseModels 
- Interfaces 
- Constants
- Extensions
- Interfaces
- Enums

#### Data
- DbContext  
- Initialize
- Entities  
- Fluent API Configurations

#### Logging
- Log 

#### Process
- Mapping 
- Repository 

#### Shared
- Resources (Mapped Models  , etc.)

#### Api
- Api
- Project Files
