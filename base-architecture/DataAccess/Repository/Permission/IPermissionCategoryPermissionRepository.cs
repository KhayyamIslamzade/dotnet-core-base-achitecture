﻿using Domain.Entities.Identity;

namespace DataAccess.Repository.Permission
{
    public interface IPermissionCategoryPermissionRepository : IRepository<PermissionCategoryPermission>
    {
    }
}
