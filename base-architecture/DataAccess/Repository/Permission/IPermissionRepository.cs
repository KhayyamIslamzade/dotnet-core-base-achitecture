﻿namespace DataAccess.Repository.Permission
{
    public interface IPermissionRepository : IRepository<Domain.Entities.Identity.Permission>
    {
    }
}
