﻿using Data;

namespace DataAccess.Repository.Permission
{
    public class PermissionRepository : Repository<Domain.Entities.Identity.Permission>, IPermissionRepository, IRepositoryIdentifier
    {
        public PermissionRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
