﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DataAccess.Repository.Role
{
    public class RoleRepository : IRoleRepository, IRepositoryIdentifier
    {
        private readonly RoleManager<Domain.Entities.Identity.Role> _roleManager;

        public RoleRepository(RoleManager<Domain.Entities.Identity.Role> roleManager)
        {
            _roleManager = roleManager;
        }


        public Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId)
        {
            return _roleManager.FindByIdAsync(roleId);
        }

        public Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId, params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties)
        {
            var query = _roleManager.Roles.IncludeAll(includeProperties);
            return query.FirstOrDefaultAsync(c => c.Id == roleId);
        }


        public Task<Domain.Entities.Identity.Role> GetRoleByIdAsync(string roleId, params string[] includeProperties)
        {
            var query = _roleManager.Roles.IncludeAll(includeProperties);

            return query.FirstOrDefaultAsync(c => c.Id == roleId);
        }
        public IQueryable<Domain.Entities.Identity.Role> FindBy(Expression<Func<Domain.Entities.Identity.Role, bool>> predicate)
        {
            return _roleManager.Roles.Where(predicate);
        }
        public IQueryable<Domain.Entities.Identity.Role> FindBy(Expression<Func<Domain.Entities.Identity.Role, bool>> predicate, params string[] includeProperties)
        {
            return _roleManager.Roles.IncludeAll(includeProperties).Where(predicate);
        }
        public IQueryable<Domain.Entities.Identity.Role> FindBy(Expression<Func<Domain.Entities.Identity.Role, bool>> predicate, params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties)
        {
            return _roleManager.Roles.IncludeAll(includeProperties).Where(predicate);
        }

        public IQueryable<Domain.Entities.Identity.Role> GetAll()
        {
            return _roleManager.Roles;
        }
        public IQueryable<Domain.Entities.Identity.Role> GetAll(params string[] includeProperties)
        {
            return _roleManager.Roles.IncludeAll(includeProperties);
        }
        public IQueryable<Domain.Entities.Identity.Role> GetAll(params Expression<Func<Domain.Entities.Identity.Role, object>>[] includeProperties)
        {
            return _roleManager.Roles.IncludeAll(includeProperties);
        }

        public Task<bool> IsExistAsync(Expression<Func<Domain.Entities.Identity.Role, bool>> predicate)
        {
            return _roleManager.Roles.AnyAsync(predicate);
        }
        public Task<IdentityResult> CreateAsync(Domain.Entities.Identity.Role role)
        {
            return _roleManager.CreateAsync(role);
        }
        public Task<IdentityResult> UpdateAsync(Domain.Entities.Identity.Role role)
        {
            return _roleManager.UpdateAsync(role);
        }

        public Task<IdentityResult> DeleteAsync(Domain.Entities.Identity.Role role)
        {
            return _roleManager.DeleteAsync(role);
        }

    }
}
