﻿namespace Shared.Resources.User
{
    public class UserLoginResponse
    {
        public string Token { get; set; }
    }
}
