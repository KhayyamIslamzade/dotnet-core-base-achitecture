﻿using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Core.Extensions;
using Core.Utilities;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.User;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Application.Services.Identity;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly UserService _userService;
        private readonly AuthService _authService;
        private readonly SignInManager<User> _signInManager;
        private readonly TokenService _tokenService;

        public AccountController(UserService userService, AuthService authService, SignInManager<User> signInManager, IMapper mapper,
            TokenService tokenService)
        {
            _userService = userService;
            _authService = authService;
            _signInManager = signInManager;
            _mapper = mapper;
            _tokenService = tokenService;
        }
        [HttpPost("GetToken")]
        public async Task<ApiResponse> GetTokenAsync([FromBody] UserLoginData data)
        {

            var isEmail = data.EmailOrUsername.IsEmail();
            var isUsername = data.EmailOrUsername.IsUsername();

            if (!(isEmail || isUsername))
                ModelState.AddModelError("emailOrUsername", "Enter valid Email/Username");

            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await _tokenService.GetTokenAsync(data.EmailOrUsername, isUsername, data.Password);

            return new ApiResponse(new
            {
                Token = result
            });
        }
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("GetAuthorizedUser")]
        public async Task<ApiResponse> GetAuthorizedAsync()
        {
            var result = await _userService.GetAuthUserMappedAsync();
            return new ApiResponse(result);

        }

        [HttpPost("Register")]
        public async Task<ApiResponse> Register([FromBody] UserRegisterData data)
        {

            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            await _userService.CreateAsync(new UserAddData()
            {
                UserName = data.UserName,
                Email = data.Email,
                Password = data.Password,
                ConfirmPassword = data.ConfirmPassword
            });
            return new ApiResponse(MessageBuilder.UserOrEmailExist, StatusCodes.Status400BadRequest);
        }

        [HttpGet("ConfirmEmail")]
        public async Task<ApiResponse> ConfirmEmail([FromQuery][Required] string userId, [FromQuery][Required] string token)
        {
            await _authService.ConfirmEmailAsync(userId, token).ConfigureAwait(false);
            return new ApiResponse(MessageBuilder.EmailConfirmed, StatusCodes.Status200OK);
        }

        [HttpGet("ForgotPassword")]
        public async Task<ApiResponse> ForgotPassword([Required] string email)
        {
            await _authService.ForgotPasswordAsync(email);
            return new ApiResponse();
        }
        [HttpGet("CheckPasswordReset")]
        public async Task<ApiResponse> PasswordReset([FromQuery][Required] string userId)
        {
            await _authService.CheckPasswordResetRequestAsync(userId);
            return new ApiResponse(MessageBuilder.Success, StatusCodes.Status200OK);
        }
        [HttpPost("PasswordReset")]
        public async Task<ApiResponse> PasswordReset(PasswordResetData data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors(), StatusCodes.Status400BadRequest);

            await _authService.PasswordResetAsync(data);
            return new ApiResponse(MessageBuilder.PasswordReset, StatusCodes.Status200OK);
        }

    }
}
