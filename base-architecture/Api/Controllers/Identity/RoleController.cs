﻿using Application.Services;
using AutoMapper;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.Role;
using System.Threading.Tasks;
using Application.Services.Identity;

namespace Api.Controllers.Identity
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class RoleController : ControllerBase
    {
        private readonly RoleService _roleService;

        public RoleController(RoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetAsync(string id)
        {
            return new ApiResponse(await _roleService.GetMappedAsync(id));

        }

        [HttpGet]
        public async Task<ApiResponse> GetAllAsync()
        {

            return new ApiResponse(await _roleService.GetAllMappedAsync());
        }

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] RoleData data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _roleService.CreateAsync(data).ConfigureAwait(false);

            return await GetAsync(result.Id).ConfigureAwait(false);
        }

        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] RoleData data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _roleService.CreateAsync(data).ConfigureAwait(false);
            return await GetAsync(result.Id).ConfigureAwait(false);
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(string id)
        {
            await _roleService.DeleteAsync(id);
            return new ApiResponse(result: null);
        }

    }
}
