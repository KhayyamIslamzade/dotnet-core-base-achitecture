﻿using Application.Services;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared.Resources.User;
using System.Threading.Tasks;
using Application.Services.Identity;

namespace Api.Controllers.Identity
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetAsync(string id)
        {
            var result = await _userService.GetAsync(id);
            return new ApiResponse(result);

        }
        [HttpGet]
        public async Task<ApiResponse> GetAllAsync()
        {
            var result = await _userService.GetAllMappedAsync();
            return new ApiResponse(result);

        }

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] UserAddData data)
        {

            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _userService.CreateAsync(data);
            return new ApiResponse(await GetAsync(result.Id));

        }
        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] UserEditData data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            var result = await _userService.UpdateAsync(data);
            return new ApiResponse(await GetAsync(result.Id));
        }


        [HttpPost("ChangePassword")]
        public async Task<ApiResponse> ChangePasswordAsync([FromBody] UserChangePasswordData data)
        {
            if (!ModelState.IsValid)
                return new ApiResponse(ModelState.AllErrors());

            await _userService.ChangePasswordAsync(data);
            return new ApiResponse(result: null);

        }


        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(string id)
        {
            await _userService.DeleteAsync(id);
            return new ApiResponse(result: null);
        }

    }
}
