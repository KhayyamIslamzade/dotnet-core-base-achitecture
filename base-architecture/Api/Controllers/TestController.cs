﻿using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    public class TestController : ControllerBase
    {
        [HttpGet("apiexception")]

        //[Permission(true, "admin", "moderator")]
        public ApiResponse apiexception()
        {
            throw new Exception("test", innerException: new Exception("test"));
        }
        [HttpGet("modelexception")]
        public ApiResponse modelexception()
        {
            ModelState.AddModelError("test", "error");
            //throw new Exception("test");
            throw new ApiException(ModelState.AllErrors());
        }
        [HttpGet("result")]
        public ApiResponse result()
        {
            return new ApiResponse(new { test = "dsadsad", xeyyam = "dasdasdsa" }, StatusCodes.Status200OK);
        }
    }
}
