﻿using Domain.Common.Enums;
using System;

namespace Domain.Common.Configurations
{
    public class Entity : IEntity
    {
        public RecordStatus Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }

        public Entity()
        {
            Status = RecordStatus.Active;
        }
    }
}
