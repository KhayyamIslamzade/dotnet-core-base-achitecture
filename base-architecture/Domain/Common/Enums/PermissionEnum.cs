﻿

namespace Domain.Common.Enums
{
    public enum PermissionEnum
    {
        Add,
        Edit,
        List,
        Delete,


        //directive
        Admin,
        Moderator
    }
}
