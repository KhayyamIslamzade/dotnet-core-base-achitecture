﻿using Domain.Common.Configurations;
using Domain.Common.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Domain.Entities.Identity
{

    public class User : IdentityUser<string>, IEntity
    {

        public ICollection<UserRole> Roles { get; set; } = new Collection<UserRole>();
        public ICollection<UserPermission> DirectivePermissions { get; set; } = new Collection<UserPermission>();

        public ICollection<EmailConfirmationRequest> EmailConfirmationRequests { get; set; } = new Collection<EmailConfirmationRequest>();
        public ICollection<PasswordResetRequest> PasswordResetRequests { get; set; } = new Collection<PasswordResetRequest>();

        public bool IsEditable { get; set; } = true;

        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public RecordStatus Status { get; set; }
    }
}
