﻿using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Http;

namespace Core.Exceptions
{
    public class InternalServerException : ApiException
    {

        public InternalServerException(string detail, string title = "Sorry, an unexpected error has occurred") : base(title, StatusCodes.Status500InternalServerError)
        {
        }
    }
}
