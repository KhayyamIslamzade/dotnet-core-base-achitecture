﻿using AutoMapper;
using DataAccess.Repository.Permission;
using Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Resources.Permission;
using Shared.Resources.PermissionCategory;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Identity
{
    public class PermissionService
    {
        private readonly IPermissionCategoryPermissionRepository _repository;
        private readonly IMapper _mapper;
        private readonly IPermissionRepository _permissionRepository;

        public PermissionService(IPermissionCategoryPermissionRepository repository, IMapper mapper, IPermissionRepository permissionRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _permissionRepository = permissionRepository;
        }

        public async Task<List<PermissionCategoryRelationGetData>> GetAllAsync()
        {
            var data = _repository.GetAll("Permission", "Category");
            var result =
                _mapper.Map<List<PermissionCategoryPermission>, List<PermissionCategoryRelationGetData>>(await data.ToListAsync()
                    .ConfigureAwait(false));

            return result;
        }

        public async Task<List<PermissionGetData>> GetAllDirectivePermissionsAsync()
        {
            var data = _permissionRepository.FindBy(c => c.IsDirective);
            var result =
                _mapper.Map<List<Permission>, List<PermissionGetData>>(await data.ToListAsync()
                    .ConfigureAwait(false));
            return result;
        }
    }
}
