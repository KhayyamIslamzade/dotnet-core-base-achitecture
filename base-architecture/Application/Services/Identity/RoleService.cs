﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Core.Constants;
using Core.Exceptions;
using DataAccess.Repository.Role;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Resources.Role;

namespace Application.Services.Identity
{
    public class RoleService
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly IRoleRepository _repository;
        private readonly IMapper _mapper;

        public RoleService(IRoleRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Role> GetAsync(string id)
        {
            var includeParams = new IncludeStringConstants().RolePermissionIncludeList;
            includeParams.Add("Users.User");
            var role = await _repository.GetRoleByIdAsync(id, includeParams.ToArray()).ConfigureAwait(false);
            if (role == null)
                throw new RecordNotFoundException();

            return role;
        }
        public async Task<RoleGetData> GetMappedAsync(string id)
        {
            var data = _mapper.Map<Role, RoleGetData>(await GetAsync(id));
            return data;
        }

        public async Task<List<Role>> GetAllAsync()
        {
            var includeParams = new IncludeStringConstants().RolePermissionIncludeList;
            includeParams.Add("Users.User");
            var role = await _repository.FindBy(c => c.IsEditable, includeParams.ToArray()).ToListAsync().ConfigureAwait(false);
            return role;
        }

        public async Task<List<RoleGetData>> GetAllMappedAsync()
        {
            var data = _mapper.Map<List<Role>, List<RoleGetData>>(await GetAllAsync());
            return data;
        }

        public async Task<Role> CreateAsync(RoleData data)
        {
            var isExistAsync = await _repository.IsExistAsync(c => c.Name == data.Name).ConfigureAwait(false);
            if (isExistAsync)
                throw new RecordAlreadyExistException();

            var role = _mapper.Map<RoleData, Role>(data);
            role.Id = Guid.NewGuid().ToString();
            role.DateCreated = DateTime.Now;
            await _repository.CreateAsync(role).ConfigureAwait(false);

            return role;
        }

        public async Task<Role> UpdateAsync(RoleData data)
        {
            var role = await _repository.GetRoleByIdAsync(data.Id, new IncludeStringConstants().RolePermissionIncludeList.ToArray()).ConfigureAwait(false);
            if (role == null)
                throw new RecordNotFoundException();
            if (!role.IsEditable)
                throw new RecordNotEditableException();

            var isExistAsync = data.Name != role.Name &&
                               await _repository.IsExistAsync(c => c.Name == data.Name).ConfigureAwait(false);
            if (isExistAsync)
                throw new RecordAlreadyExistException();

            //update
            _mapper.Map<RoleData, Role>(data, role);
            await _repository.UpdateAsync(role).ConfigureAwait(false);

            return role;
        }

        public async Task DeleteAsync(string id)
        {
            var role = await _repository.GetRoleByIdAsync(id).ConfigureAwait(false);
            if (role == null)
                throw new RecordNotFoundException();

            if (!role.IsEditable)
                throw new RecordNotEditableException();

            await _repository.DeleteAsync(role).ConfigureAwait(false);
        }


    }
}
