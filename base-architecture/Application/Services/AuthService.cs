﻿using AutoWrapper.Wrappers;
using Core.Constants;
using Core.Utilities;
using DataAccess.Repository.User;
using Domain.Common.Enums;
using Domain.Entities.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Shared.Resources.User;
using System;
using System.Linq;
using System.Threading.Tasks;
using Application.Services.Identity;

namespace Application.Services
{
    public class AuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly UserService _userService;
        private readonly ConfirmationService _confirmationService;
        private readonly UserManager<User> _userManager;

        public AuthService(IUserRepository userRepository, UserService userService, ConfirmationService confirmationService, UserManager<User> userManager)
        {
            _userRepository = userRepository;
            _userService = userService;
            _confirmationService = confirmationService;
            _userManager = userManager;
        }

        public bool IsAdmin(User user)
        {
            return UserIsInPermission(user, nameof(PermissionEnum.Admin));
        }
        public async Task<bool> IsAdminAsync()
        {
            var userId = _userService.GetAuthorizedUserId();
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            return UserIsInPermission(await _userRepository.GetUserByIdAsync(userId, includeParams.ToArray()), nameof(PermissionEnum.Admin));
        }
        public async Task<bool> IsAdminAsync(string userId)

        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            return UserIsInPermission(await _userRepository.GetUserByIdAsync(userId, includeParams.ToArray()), nameof(PermissionEnum.Admin));
        }
        public async Task<bool> UserIsInRoleAsync(User user, string roleName)
        {
            var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            return roles.Any(s => s.Equals(roleName, StringComparison.OrdinalIgnoreCase));
        }

        public bool UserIsInPermission(User user, string permissionName)
        {
            var directivePermissions = user.DirectivePermissions.Select(c => c.Permission.Label).ToList();
            var userRole = user.Roles.Select(c => c.Role).ToList();
            var permissions = userRole.SelectMany(c => c.PermissionCategory.Select(e => e.PermissionCategoryPermission.Permission.Label)).ToList();

            return directivePermissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase)) ||
                   permissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase));


        }
        public async Task<bool> UserIsInPermissionAsync(string userId, string permissionName)
        {
            var includeParams = new IncludeStringConstants().UserRolePermissionIncludeArray.ToList();
            var user = await _userRepository.GetUserByIdAsync(userId, includeParams.ToArray()).ConfigureAwait(false);
            var directivePermissions = user.DirectivePermissions.Select(c => c.Permission.Label).ToList();
            var userRole = user.Roles.Select(c => c.Role).ToList();
            var permissions = userRole.SelectMany(c => c.PermissionCategory.Select(e => $"{ e.PermissionCategoryPermission.Category.Label.ToLower()}_{ e.PermissionCategoryPermission.Permission.Label.ToLower()}")).ToList();

            return directivePermissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase)) ||
                   permissions.Any(c => c.Equals(permissionName, StringComparison.OrdinalIgnoreCase));
        }




        public async Task ConfirmEmailAsync(string userId, string token)
        {
            if (!string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(token))
            {
                var user = await _userService.GetAsync(userId);
                if (user.EmailConfirmed)
                    throw new ApiException(MessageBuilder.EmailAlreadyConfirmed, StatusCodes.Status400BadRequest);

                var result = await _userRepository.ConfirmEmailAsync(userId, token);
                if (result.Succeeded)
                {
                    await _userRepository.SetLastEmailRequestDeleted(userId).ConfigureAwait(false);
                    //return new ApiResponse(MessageBuilder.EmailConfirmed, StatusCodes.Status200OK);

                }
            }
            throw new ApiException(MessageBuilder.InvalidToken);
        }

        public async Task ForgotPasswordAsync(string email)
        {
            var user = await _userRepository.GetUserByEmailAsync(email);
            if (user == null)
                throw new ApiException(MessageBuilder.NotFound, StatusCodes.Status400BadRequest);
            await _confirmationService.SendPasswordResetMailAsync(user.Id, user.Email).ConfigureAwait(false);
        }

        public async Task CheckPasswordResetRequestAsync(string userId)
        {
            var user = await _userRepository.GetUserByIdAsync(userId, c => c.PasswordResetRequests);
            if (!user.PasswordResetRequests.Any())
                throw new ApiException(MessageBuilder.InvalidToken);

        }
        public async Task PasswordResetAsync(PasswordResetData data)
        {
            var result = await _userRepository.ResetPasswordAsync(data.UserId, data.Token, data.Password);
            if (result.Succeeded)
                await _userRepository.SetLastPasswordRequestDeleted(data.UserId);
            throw new ApiException(MessageBuilder.InvalidToken);
        }


    }
}
