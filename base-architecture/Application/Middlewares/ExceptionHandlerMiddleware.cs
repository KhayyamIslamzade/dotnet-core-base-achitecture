﻿using Application.Services;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using System.Threading.Tasks;
using Application.Services.Identity;

namespace Application.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;


        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            if (next == null) throw new ArgumentNullException(nameof(next));
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, UserService userService)
        {

            if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));

            try
            {
                await _next(httpContext).ConfigureAwait(false);
            }
            catch (Exception exception)
            {
                var errorId = Guid.NewGuid();
                Log.ForContext("Type", "Error").ForContext("Exception", exception, true)
                    .Error(exception, exception.Message + ". {@errorId}", errorId);
                string jsonResult = "";
                var isDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development;
                //if (!(exception is ApiException))
                //{
                var ex = isDevelopment
                    ? new ApiError(exception.Message)
                    : new ApiError("Sorry, an unexpected error has occurred");
                DefaultContractResolver contractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
                var apiResponse = new ApiResponse(StatusCodes.Status500InternalServerError, ex);
                apiResponse.Message = "Error";



                jsonResult = JsonConvert.SerializeObject(apiResponse,
                    settings: new JsonSerializerSettings()
                    {
                        ContractResolver = contractResolver,

                    });
                await httpContext.Response.WriteAsync(jsonResult);

            }
        }

    }
}
